#!/bin/bash
if pacman -Qs garuda-fish-config; then
    cp /etc/skel/.config/fish/config.fish ~/.config/fish/config.fish && chsh -s /usr/bin/fish
else sudo pacman -S garuda-fish-config && cp /etc/skel/.config/fish/config.fish ~/.config/fish/config.fish && chsh -s /usr/bin/fish
fi
