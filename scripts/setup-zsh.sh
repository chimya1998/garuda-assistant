#!/bin/bash
if pacman -Qs garuda-zsh-config; then
    cp /etc/skel/.zshrc ~ && chsh -s /usr/bin/fish
else sudo pacman -S garuda-zsh-config && cp /etc/skel/.zshrc ~ && chsh -s /usr/bin/zsh
fi
