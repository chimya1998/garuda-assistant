#!/bin/bash
if [[ $(cat /etc/hosts | grep -A1 "Blocked domains" | awk '/Blocked domains/ { print $NF }') > "1" ]]; then
    echo "enabled"
elif [ $(cat /etc/hosts | grep -A1 "Blocked domains" | awk '/Blocked domains/ { print $NF }') == "0" ]; then
    echo "disabled"
else
    echo "disabled"
fi
