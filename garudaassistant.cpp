#include "garudaassistant.h"
#include "ui_garudaassistant.h"
#include <QDebug>
#include <QFile>

GarudaAssistant::GarudaAssistant(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::GarudaAssistant)
{
    ui->setupUi(this);
    setup();
    check_status();
    on_toolButton_diskRefresh_clicked();
    //check_status_qwickaccess();
    //check_status_control();
}

GarudaAssistant::~GarudaAssistant()
{
    delete ui;
}

// Util function for getting bash command output and error code
Result GarudaAssistant::runCmd(QString cmd, bool include_stderr)
{
    QProcess proc(this);
    if (include_stderr)
    proc.setProcessChannelMode(QProcess::MergedChannels);
    proc.start("/bin/bash", QStringList() << "-c" << cmd);
    // Wait forever
    proc.waitForFinished(-1);
    Result result = { proc.exitCode(), proc.readAll().trimmed() };
    return result;
}

// disconnect all connections
void GarudaAssistant::disconnectAll()
{
    disconnect(proc, SIGNAL(started()), 0, 0);
    disconnect(proc, SIGNAL(finished(int)), 0, 0);
}

// set proc and timer connections
void GarudaAssistant::setConnections()
{
    connect(proc, SIGNAL(started()), SLOT(procStart()));
    connect(proc, SIGNAL(finished(int)), SLOT(procDone(int)));
}

// setup versious items first time program runs
void GarudaAssistant::setup()
{
    this->setWindowTitle(tr("Garuda Assistant"));
    QString gdm = runCmd("pacman -Qq gdm").output;
    if (system("[ -f /usr/sbin/gdm ]") != 0) {
        ui->groupBox_gdm->hide();
    }
    //this->adjustSize();

}

bool GarudaAssistant::checkAndInstall(QString package)
{
    // Not async, don't really care either tho, not my problem
    // Assumption: package is a safe string
    auto output = runCmd("pacman -Qq " + package, false);

    // If it's already installed, we are good to go
    if (output.output == package || output.output == package + "-git")
        return true;

    if (!QFile::exists("/usr/bin/pamac-installer")) {
        QMessageBox::warning(this, tr("Error while starting ") + package, tr("The package \"%package%\" is not installed and pamac-installer is missing.").replace("%package%", package));
        return false;
    }

    this->hide();
    runCmd("pamac-installer " + package);

    // Checking pamac-installer's exit code alone is not enough. For some reason it decides to return 0 even tho it failed sometimes
    output = runCmd("pacman -Qq " + package,  false);
    if (output.output == package)
        return true;
    else {
        this->show();
        return false;
    }
}

void GarudaAssistant::check_status()
{
    QProcess proc;

    proc.start("pacman -Qq gstreamer-meta");
    proc.waitForFinished();
        QString gstreamer=proc.readAllStandardOutput();
        gstreamer = gstreamer.trimmed();

    if (gstreamer == "gstreamer-meta"){
        ui->checkBox_gstreamer->setChecked(true);
    }
    else
    {
        ui->checkBox_gstreamer->setChecked(false);
    }

    proc.start("/bin/bash", QStringList()<< "/usr/share/garuda/scripts/check-video.sh");
    proc.waitForFinished();
    QString video=proc.readAllStandardOutput();
    video = video.trimmed();
    if (video == "enabled"){
             ui->checkBox_video->setChecked(true);
    }
    else
    {
             ui->checkBox_video->setChecked(false);
    }

    proc.start("pacman -Qq systemd-guest-user");
    proc.waitForFinished();
        QString guest=proc.readAllStandardOutput();
        guest = guest.trimmed();
       // qDebug()<< guest;
    if (guest == "systemd-guest-user"){
        ui->checkBox_guest->setChecked(true);
    }
    else
    {
        ui->checkBox_guest->setChecked(false);
    }

    proc.start("/bin/bash", QStringList()<< "/usr/share/garuda/scripts/check-adguard.sh");
    proc.waitForFinished();
        QString adguard=proc.readAllStandardOutput();
        adguard = adguard.trimmed();
        //qDebug()<< adguard;
        if (adguard == "enabled"){
            ui->checkBox_adguard->setChecked(true);
        }
        else
        {
            ui->checkBox_adguard->setChecked(false);
        }

        proc.start("/bin/bash", QStringList()<< "/usr/share/garuda/scripts/check-hblock.sh");
        proc.waitForFinished();
            QString hblock=proc.readAllStandardOutput();
            hblock = hblock.trimmed();
            qDebug()<< hblock;
            if (hblock == "enabled"){
                ui->checkBox_hblock->setChecked(true);
            }
            else
            {
                ui->checkBox_hblock->setChecked(false);
            }

        proc.start("pacman -Qq printer-support");
        proc.waitForFinished();
            QString printer=proc.readAllStandardOutput();
            printer = printer.trimmed();
           // qDebug()<< printer;
        if (printer == "printer-support"){
            ui->checkBox_printer->setChecked(true);
        }
        else
        {
            ui->checkBox_printer->setChecked(false);
        }

        proc.start("/bin/bash", QStringList()<< "/usr/share/garuda/scripts/check-cups.sh");
        proc.waitForFinished();
        QString cups=proc.readAllStandardOutput();
        cups = cups.trimmed();
        //qDebug()<< sambashare;
        if (cups == "enabled"){
                 ui->checkBox_cupsgrp->setChecked(true);
        }
        else
        {
                 ui->checkBox_cupsgrp->setChecked(false);
        }
        
        proc.start("/bin/bash", QStringList()<< "/usr/share/garuda/scripts/check-sys.sh");
        proc.waitForFinished();
        QString sys=proc.readAllStandardOutput();
        sys = sys.trimmed();
        //qDebug()<< sambashare;
        if (sys == "enabled"){
                 ui->checkBox_sysgrp->setChecked(true);
        }
        else
        {
                 ui->checkBox_sysgrp->setChecked(false);
        }

        proc.start("systemctl", QStringList()<< "is-enabled" << "cups.socket" << "--no-ask-password");
        proc.waitForFinished();
            QString cupsstatus=proc.readAllStandardOutput();
            cupsstatus = cupsstatus.trimmed();
            if (cupsstatus == "enabled"){
                ui->checkBox_cups->setChecked(true);
            }
            else
            {
                ui->checkBox_cups->setChecked(false);
            }

            proc.start("pacman -Qq scanner-support");
            proc.waitForFinished();
                QString scanner=proc.readAllStandardOutput();
                scanner = scanner.trimmed();
              //  qDebug()<< scanner;
            if (scanner == "scanner-support"){
                ui->checkBox_scanning->setChecked(true);
            }
            else
            {
                ui->checkBox_scanning->setChecked(false);
            }


            proc.start("systemctl", QStringList()<< "is-enabled" << "ipp-usb" << "--no-ask-password");
            proc.waitForFinished();
                QString usbipp=proc.readAllStandardOutput();
                usbipp = usbipp.trimmed();
                if (usbipp == "enabled"){
                ui->checkBox_usbipp->setChecked(true);
            }
            else
            {
                ui->checkBox_usbipp->setChecked(false);
            }

            proc.start("systemctl", QStringList()<< "is-enabled" << "saned.socket" << "--no-ask-password");
            proc.waitForFinished();
                QString saned=proc.readAllStandardOutput();
                saned = saned.trimmed();
                if (saned == "enabled"){
                ui->checkBox_saned->setChecked(true);
            }
            else
            {
                ui->checkBox_saned->setChecked(false);
            }
            
        proc.start("/bin/bash", QStringList()<< "/usr/share/garuda/scripts/check-scanner.sh");
        proc.waitForFinished();
        QString scannergrp=proc.readAllStandardOutput();
        scannergrp = scannergrp.trimmed();
        //qDebug()<< sambashare;
        if (scannergrp == "enabled"){
                 ui->checkBox_scannergrp->setChecked(true);
        }
        else
        {
                 ui->checkBox_scannergrp->setChecked(false);
        }


            proc.start("pacman -Qq samba-support");
            proc.waitForFinished();
                 QString samba=proc.readAllStandardOutput();
                    samba = samba.trimmed();
                  //  qDebug()<< samba;
            if (samba == "samba-support"){
                    ui->checkBox_samba->setChecked(true);
            }
            else
              {
                    ui->checkBox_samba->setChecked(false);
              }


            proc.start("/bin/bash", QStringList()<< "/usr/share/garuda/scripts/check-sambashare.sh");
            proc.waitForFinished();
            QString sambashare=proc.readAllStandardOutput();
            sambashare = sambashare.trimmed();
            //qDebug()<< sambashare;
            if (sambashare == "enabled"){
                     ui->checkBox_sambashare->setChecked(true);
            }
            else
            {
                     ui->checkBox_sambashare->setChecked(false);
            }

            proc.start("systemctl", QStringList()<< "is-enabled" << "smb" << "--no-ask-password");
            proc.waitForFinished();
            QString smbstatus=proc.readAllStandardOutput();
            smbstatus = smbstatus.trimmed();
            if (smbstatus == "enabled"){
                            ui->checkBox_smb->setChecked(true);
            }
            else
            {
                            ui->checkBox_smb->setChecked(false);
            }

            proc.start("systemctl", QStringList()<< "is-enabled" << "nmb" << "--no-ask-password");
            proc.waitForFinished();
            QString nmbstatus=proc.readAllStandardOutput();
            nmbstatus = nmbstatus.trimmed();
            if (nmbstatus == "enabled"){
                                ui->checkBox_nmb->setChecked(true);
            }
             else
            {
                                ui->checkBox_nmb->setChecked(false);
            }

            proc.start("pacman -Qq firewalld");
            proc.waitForFinished();
                QString firewalld=proc.readAllStandardOutput();
                firewalld = firewalld.trimmed();
            if (firewalld == "firewalld"){
                ui->checkBox_firewalld->setChecked(true);
            }
            else
            {
                ui->checkBox_firewalld->setChecked(false);
            }

            proc.start("systemctl", QStringList()<< "is-enabled" << "firewalld" << "--no-ask-password");
            proc.waitForFinished();
            QString firewalldstatus=proc.readAllStandardOutput();
            firewalldstatus = firewalldstatus.trimmed();
            if (firewalldstatus == "enabled"){
                                ui->checkBox_firewalldservice->setChecked(true);
            }
             else
            {
                                ui->checkBox_firewalldservice->setChecked(false);
            }

            proc.start("pacman -Qq ufw");
            proc.waitForFinished();
            QString ufw=proc.readAllStandardOutput();
            ufw = ufw.trimmed();
            if (ufw == "ufw"){
                            ui->checkBox_ufw->setChecked(true);
            }
            else
            {
                            ui->checkBox_ufw->setChecked(false);
            }

            proc.start("systemctl", QStringList()<< "is-enabled" << "ufw" << "--no-ask-password");
            proc.waitForFinished();
            QString ufwstatus=proc.readAllStandardOutput();
            ufwstatus = ufwstatus.trimmed();
            if (ufwstatus == "enabled"){
                                            ui->checkBox_ufwservice->setChecked(true);
            }
             else
            {
                                            ui->checkBox_ufwservice->setChecked(false);
            }


            proc.start("pacman -Qq fcitx-input-support");
            proc.waitForFinished();
                QString fcitx=proc.readAllStandardOutput();
                fcitx = fcitx.trimmed();
            if (fcitx == "fcitx-input-support"){
                ui->checkBox_fcitx->setChecked(true);
            }
            else
            {
                ui->checkBox_fcitx->setChecked(false);
            }

            proc.start("pacman -Qq fcitx5-input-support");
            proc.waitForFinished();
                QString fcitx5=proc.readAllStandardOutput();
                fcitx5 = fcitx5.trimmed();
            if (fcitx5 == "fcitx5-input-support"){
                ui->checkBox_fcitx5->setChecked(true);
            }
            else
            {
                ui->checkBox_fcitx5->setChecked(false);
            }

            proc.start("pacman -Qq ibus-input-support");
            proc.waitForFinished();
                QString ibus=proc.readAllStandardOutput();
                ibus = ibus.trimmed();
            if (ibus == "ibus-input-support"){
                ui->checkBox_ibus->setChecked(true);
            }
            else
            {
                ui->checkBox_ibus->setChecked(false);
            }
            proc.start("pacman -Qq asian-fonts");
            proc.waitForFinished();
                QString asian=proc.readAllStandardOutput();
                asian = asian.trimmed();
            if (asian == "asian-fonts"){
                ui->checkBox_asian->setChecked(true);
            }
            else
            {
                ui->checkBox_asian->setChecked(false);
            }

            proc.start("pacman -Qq evdev-right-click-emulation");
            proc.waitForFinished();
                QString lptrce=proc.readAllStandardOutput();
                lptrce = lptrce.trimmed();
            if (lptrce == "evdev-right-click-emulation"){
                ui->checkBox_lptrce->setChecked(true);
            }
            else
            {
                ui->checkBox_lptrce->setChecked(false);
            }
            proc.start("systemctl", QStringList()<< "is-enabled" << "evdev-rce" << "--no-ask-password");
            proc.waitForFinished();
                QString evdevrce=proc.readAllStandardOutput();
                evdevrce = evdevrce.trimmed();
                if (evdevrce == "enabled"){
                    ui->checkBox_evdevrce->setChecked(true);
                }
                else
                {
                    ui->checkBox_evdevrce->setChecked(false);
                }

                proc.start("pacman -Qq virt-manager-meta");
                                proc.waitForFinished();
                                     QString virtmanager=proc.readAllStandardOutput();
                                        virtmanager = virtmanager.trimmed();
                                      //  qDebug()<< virtmanager;
                                if (virtmanager == "virt-manager-meta"){
                                        ui->checkBox_virtmanager->setChecked(true);
                                }
                                else
                                  {
                                        ui->checkBox_virtmanager->setChecked(false);
                                  }


                                proc.start("/bin/bash", QStringList()<< "/usr/share/garuda/scripts/check-libvirt.sh");
                                proc.waitForFinished();
                                QString libvirtgrp=proc.readAllStandardOutput();
                                libvirtgrp = libvirtgrp.trimmed();
                                //qDebug()<< libvirt;
                                if (libvirtgrp == "enabled"){
                                         ui->checkBox_libvirtgrp->setChecked(true);
                                }
                                else
                                {
                                         ui->checkBox_libvirtgrp->setChecked(false);
                                }

                                proc.start("systemctl", QStringList()<< "is-enabled" << "libvirtd" << "--no-ask-password");
                                proc.waitForFinished();
                                QString libvirtdstatus=proc.readAllStandardOutput();
                                libvirtdstatus = libvirtdstatus.trimmed();
                                if (libvirtdstatus == "enabled"){
                                                ui->checkBox_libvirtd->setChecked(true);
                                }
                                else
                                {
                                                ui->checkBox_libvirtd->setChecked(false);
                                }

                                proc.start("pacman -Qq virtualbox-meta");
                                                               proc.waitForFinished();
                                                                    QString virtualbox=proc.readAllStandardOutput();
                                                                       virtualbox = virtualbox.trimmed();
                                                                     //  qDebug()<< virtualbox;
                                                               if (virtualbox == "virtualbox-meta"){
                                                                       ui->checkBox_virtualbox->setChecked(true);
                                                               }
                                                               else
                                                                 {
                                                                       ui->checkBox_virtualbox->setChecked(false);
                                                                 }


                                                               proc.start("/bin/bash", QStringList()<< "/usr/share/garuda/scripts/check-vboxusers.sh");
                                                               proc.waitForFinished();
                                                               QString vboxusers=proc.readAllStandardOutput();
                                                               vboxusers = vboxusers.trimmed();
                                                               //qDebug()<< libvirt;
                                                               if (vboxusers == "enabled"){
                                                                        ui->checkBox_vboxusers->setChecked(true);
                                                               }
                                                               else
                                                               {
                                                                        ui->checkBox_vboxusers->setChecked(false);
                                                               }


      /*  proc.start("/bin/bash", QStringList()<< "/usr/share/garuda/scripts/check-hidpi.sh");
        proc.waitForFinished();
            QString hidpi=proc.readAllStandardOutput();
            hidpi = hidpi.trimmed();
            //qDebug()<< hidpi;
            if (hidpi == "enabled"){
                ui->checkBox_hidpi->setChecked(true);
            }
            else
            {
                ui->checkBox_hidpi->setChecked(false);
            }

    */

                proc.start("/bin/bash", QStringList()<< "/usr/share/garuda/scripts/check-gdmwayland.sh");
                proc.waitForFinished();
                    QString gdmwayland=proc.readAllStandardOutput();
                    gdmwayland = gdmwayland.trimmed();
                    //qDebug()<< gdmwayland;
                    if (gdmwayland == "enabled"){
                        ui->checkBox_gdm->setChecked(true);
                    }
                    else
                    {
                        ui->checkBox_gdm->setChecked(false);
                    }


                    proc.start("systemctl", QStringList()<< "is-enabled" << "btrfs-trim.timer" << "--no-ask-password");
                    proc.waitForFinished();
                    QString fstrimstatus=proc.readAllStandardOutput();
                    fstrimstatus = fstrimstatus.trimmed();
                    if (fstrimstatus == "enabled"){
                        ui->checkBox_Trim->setChecked(true);
                    }
                    else
                    {
                        ui->checkBox_Trim->setChecked(false);
                    }

                    proc.start("systemctl", QStringList()<< "is-enabled" << "btrfs-scrub.timer" << "--no-ask-password");
                    proc.waitForFinished();
                        QString scrubstatus=proc.readAllStandardOutput();
                        scrubstatus = scrubstatus.trimmed();
                        if (scrubstatus == "enabled"){
                            ui->checkBox_Scrub->setChecked(true);
                        }
                        else
                        {
                            ui->checkBox_Scrub->setChecked(false);
                        }

                        proc.start("systemctl", QStringList()<< "is-enabled" << "btrfs-balance.timer" << "--no-ask-password");
                        proc.waitForFinished();
                            QString balancestatus=proc.readAllStandardOutput();
                            balancestatus = balancestatus.trimmed();
                            if (balancestatus == "enabled"){
                                ui->checkBox_Balance->setChecked(true);
                            }
                            else
                            {
                                ui->checkBox_Balance->setChecked(false);
                            }

                            proc.start("systemctl", QStringList()<< "is-enabled" << "btrfs-defrag.timer" << "--no-ask-password");
                            proc.waitForFinished();
                                QString defragstatus=proc.readAllStandardOutput();
                                defragstatus = defragstatus.trimmed();
                                if (defragstatus == "enabled"){
                                    ui->checkBox_Defrag->setChecked(true);
                                }
                                else
                                {
                                    ui->checkBox_Defrag->setChecked(false);
                                }


        proc.start("systemctl", QStringList()<< "is-enabled" << "ModemManager" << "--no-ask-password");
        proc.waitForFinished();
        QString modemservice=proc.readAllStandardOutput();
        modemservice = modemservice.trimmed();
        if (modemservice == "enabled"){
            ui->checkBox_modemmanager->setChecked(true);
        }
        else
        {
            ui->checkBox_modemmanager->setChecked(false);
        }

        proc.start("systemctl", QStringList()<< "is-enabled" << "gpsd" << "--no-ask-password");
        proc.waitForFinished();
        QString gpsd=proc.readAllStandardOutput();
        gpsd = gpsd.trimmed();
        if (gpsd == "enabled"){
            ui->checkBox_gpsd->setChecked(true);
        }
        else
        {
            ui->checkBox_gpsd->setChecked(false);
        }

        proc.start("systemctl", QStringList()<< "is-enabled" << "ofono" << "--no-ask-password");
        proc.waitForFinished();
        QString ofono=proc.readAllStandardOutput();
        ofono = ofono.trimmed();
        if (ofono == "enabled"){
            ui->checkBox_ofono->setChecked(true);
        }
        else
        {
            ui->checkBox_ofono->setChecked(false);
        }

        proc.start("systemctl", QStringList()<< "is-enabled" << "neard" << "--no-ask-password");
        proc.waitForFinished();
        QString neard=proc.readAllStandardOutput();
        neard = neard.trimmed();
        if (neard == "enabled"){
            ui->checkBox_neard->setChecked(true);
        }
        else
        {
            ui->checkBox_neard->setChecked(false);
        }

        proc.start("pacman -Qq networkmanager-support");
        proc.waitForFinished();
             QString network=proc.readAllStandardOutput();
                network = network.trimmed();

        if (network == "networkmanager-support")
        {
                ui->checkBox_network->setChecked(true);
        }
        else
        {
                ui->checkBox_network->setChecked(false);
        }

        proc.start("systemctl", QStringList()<< "is-enabled" << "NetworkManager" << "--no-ask-password");
        proc.waitForFinished();
        QString networkservice=proc.readAllStandardOutput();
        networkservice = networkservice.trimmed();
        if (networkservice == "enabled"){
            ui->checkBox_networkservice->setChecked(true);
        }
        else
        {
            ui->checkBox_networkservice->setChecked(false);
        }

        proc.start("pacman -Qq connman-support");
        proc.waitForFinished();
             QString connman=proc.readAllStandardOutput();
                connman = connman.trimmed();

        if (connman == "connman-support")
        {
                ui->checkBox_connman->setChecked(true);
        }
        else
        {
                ui->checkBox_connman->setChecked(false);
        }

        proc.start("systemctl", QStringList()<< "is-enabled" << "connman" << "--no-ask-password");
        proc.waitForFinished();
        QString connmanservice=proc.readAllStandardOutput();
        connmanservice = connmanservice.trimmed();
        if (connmanservice == "enabled"){
            ui->checkBox_connmanservice->setChecked(true);
        }
        else
        {
            ui->checkBox_connmanservice->setChecked(false);
        }
        proc.start("pacman -Qq bluetooth-support");
        proc.waitForFinished();
             QString bluetooth=proc.readAllStandardOutput();
                bluetooth = bluetooth.trimmed();
              //  qDebug()<< bluetooth;
        if (bluetooth == "bluetooth-support")
        {
                ui->checkBox_bluetooth->setChecked(true);
        }
        else
        {
                ui->checkBox_bluetooth->setChecked(false);
        }

        proc.start("systemctl", QStringList()<< "is-enabled" << "bluetooth" << "--no-ask-password");
        proc.waitForFinished();
        QString bluetoothservice=proc.readAllStandardOutput();
        bluetoothservice = bluetoothservice.trimmed();
        if (bluetoothservice == "enabled"){
            ui->checkBox_bluetoothservice->setChecked(true);
        }
        else
        {
            ui->checkBox_bluetoothservice->setChecked(false);
        }

        proc.start("systemctl", QStringList()<< "is-enabled" << "bluetooth-autoconnect" << "--no-ask-password");
        proc.waitForFinished();
        QString bluetoothauto=proc.readAllStandardOutput();
        bluetoothauto = bluetoothauto.trimmed();
        if (bluetoothauto == "enabled"){
            ui->checkBox_bluetoothauto->setChecked(true);
        }
        else
        {
            ui->checkBox_bluetoothauto->setChecked(false);
        }

        proc.start("systemctl", QStringList()<< "is-enabled" << "pulseaudio-bluetooth-autoconnect" << "--user" << "--no-ask-password");
        proc.waitForFinished();
        QString pulsebluetoothauto=proc.readAllStandardOutput();
        pulsebluetoothauto = pulsebluetoothauto.trimmed();
        if (pulsebluetoothauto == "enabled"){
            ui->checkBox_pulsebluetoothauto->setChecked(true);
        }
        else
        {
            ui->checkBox_pulsebluetoothauto->setChecked(false);
        }

        proc.start("/bin/bash", QStringList()<< "/usr/share/garuda/scripts/check-lp.sh");
        proc.waitForFinished();
        QString lp=proc.readAllStandardOutput();
        lp = lp.trimmed();
        if (lp == "enabled"){
                 ui->checkBox_lp->setChecked(true);
        }
        else
        {
                 ui->checkBox_lp->setChecked(false);
        }

        proc.start("pacman -Qq alsa-support");
        proc.waitForFinished();
            QString alsa=proc.readAllStandardOutput();
            alsa = alsa.trimmed();
           // qDebug()<< alsa;
        if (alsa == "alsa-support"){
            ui->checkBox_alsa->setChecked(true);
        }
        else
        {
            ui->checkBox_alsa->setChecked(false);
        }

        proc.start("pacman -Qq jack-support");
        proc.waitForFinished();
            QString jack=proc.readAllStandardOutput();
            jack = jack.trimmed();
           // qDebug()<< jack;
        if (jack == "jack-support"){
            ui->checkBox_jack->setChecked(true);
        }
        else
        {
            ui->checkBox_jack->setChecked(false);
        }

        proc.start("pacman -Qq pulseaudio-support");
        proc.waitForFinished();
            QString pulseaudio=proc.readAllStandardOutput();
            pulseaudio = pulseaudio.trimmed();
           // qDebug()<< pulseaudio;
        if (pulseaudio == "pulseaudio-support"){
            ui->checkBox_pulseaudio->setChecked(true);
        }
        else
        {
            ui->checkBox_pulseaudio->setChecked(false);
        }

        proc.start("pacman -Qq pipewire-support");
        proc.waitForFinished();
            QString pipewire=proc.readAllStandardOutput();
            pipewire = pipewire.trimmed();
           // qDebug()<< pipewire;
        if (pipewire == "pipewire-support"){
            ui->checkBox_pipewire->setChecked(true);
        }
        else
        {
            ui->checkBox_pipewire->setChecked(false);
        }

        proc.start("pacman -Qq performance-tweaks");
        proc.waitForFinished();
            QString performancetweaks=proc.readAllStandardOutput();
            performancetweaks = performancetweaks.trimmed();
           // qDebug()<< performancetweaks
        if (performancetweaks == "performance-tweaks"){
            ui->checkBox_performancetweaks->setChecked(true);
        }
        else
        {
            ui->checkBox_performancetweaks->setChecked(false);
        }

        proc.start("pacman -Qq powersave-tweaks");
                proc.waitForFinished();
                    QString powersavetweaks=proc.readAllStandardOutput();
                    powersavetweaks = powersavetweaks.trimmed();
                   // qDebug()<< powersavetweaks
                if (powersavetweaks == "powersave-tweaks"){
                    ui->checkBox_powersavetweaks->setChecked(true);
                }
                else
                {
                    ui->checkBox_powersavetweaks->setChecked(false);
                }

        proc.start("/bin/bash", QStringList()<< "/usr/share/garuda/scripts/check-realtime.sh");
        proc.waitForFinished();
        QString realtime=proc.readAllStandardOutput();
        realtime = realtime.trimmed();
        if (realtime == "enabled"){
                 ui->checkBox_realtime->setChecked(true);
        }
        else
        {
                 ui->checkBox_realtime->setChecked(false);
        }

        proc.start("pacman -Qq tlp");
                proc.waitForFinished();
                    QString tlp=proc.readAllStandardOutput();
                    tlp = tlp.trimmed();
                   // qDebug()<< tlp;
                if (tlp == "tlp"){
                    ui->checkBox_tlp->setChecked(true);
                }
                else
                {
                    ui->checkBox_tlp->setChecked(false);
                }

        proc.start("systemctl", QStringList()<< "is-enabled" << "tlp" << "--no-ask-password");
        proc.waitForFinished();
        QString tlpservice=proc.readAllStandardOutput();
        tlpservice = tlpservice.trimmed();
        if (tlpservice == "enabled"){
            ui->checkBox_tlpservice->setChecked(true);
        }
        else
        {
            ui->checkBox_tlpservice->setChecked(false);
        }

        proc.start("pacman -Qq auto-cpufreq");
                proc.waitForFinished();
                    QString autocpufreq=proc.readAllStandardOutput();
                    autocpufreq = autocpufreq.trimmed();
                   // qDebug()<< autocpufreq;
                if (autocpufreq == "auto-cpufreq"){
                    ui->checkBox_autocpufreq->setChecked(true);
                }
                else
                {
                    ui->checkBox_autocpufreq->setChecked(false);
                }

        proc.start("systemctl", QStringList()<< "is-enabled" << "auto-cpufreq" << "--no-ask-password");
                proc.waitForFinished();
                QString autocpufreqservice=proc.readAllStandardOutput();
                autocpufreqservice = autocpufreqservice.trimmed();
                if (autocpufreqservice == "enabled"){
                    ui->checkBox_autocpufreqservice->setChecked(true);
                }
                else
                {
                    ui->checkBox_autocpufreqservice->setChecked(false);
                }

                proc.start("pacman -Qq thermald");
                        proc.waitForFinished();
                            QString thermald=proc.readAllStandardOutput();
                            thermald = thermald.trimmed();
                           // qDebug()<< thermald;
                        if (thermald == "thermald"){
                            ui->checkBox_thermald->setChecked(true);
                        }
                        else
                        {
                            ui->checkBox_thermald->setChecked(false);
                        }

                proc.start("systemctl", QStringList()<< "is-enabled" << "thermald" << "--no-ask-password");
                        proc.waitForFinished();
                        QString thermaldservice=proc.readAllStandardOutput();
                        thermaldservice = thermaldservice.trimmed();
                        if (thermaldservice == "enabled"){
                            ui->checkBox_thermaldservice->setChecked(true);
                        }
                        else
                        {
                            ui->checkBox_thermaldservice->setChecked(false);
                        }

                        proc.start("pacman -Qq intel-undervolt");
                                proc.waitForFinished();
                                    QString intelundervolt=proc.readAllStandardOutput();
                                    intelundervolt = intelundervolt.trimmed();
                                   // qDebug()<< intelundervolt;
                                if (intelundervolt == "intel-undervolt"){
                                    ui->checkBox_intelundervolt->setChecked(true);
                                }
                                else
                                {
                                    ui->checkBox_intelundervolt->setChecked(false);
                                }

                        proc.start("systemctl", QStringList()<< "is-enabled" << "intel-undervolt" << "--no-ask-password");
                                proc.waitForFinished();
                                QString intelundervoltservice=proc.readAllStandardOutput();
                                intelundervoltservice = intelundervoltservice.trimmed();
                                if (intelundervoltservice == "enabled"){
                                    ui->checkBox_intelundervoltservice->setChecked(true);
                                }
                                else
                                {
                                    ui->checkBox_intelundervoltservice->setChecked(false);
                                }
                                proc.start("pacman -Qq uresourced");
                                 proc.waitForFinished();
                                 QString uresourced=proc.readAllStandardOutput();
                                 uresourced = uresourced.trimmed();
                                 // qDebug()<< uresourced;
                                 if (uresourced == "uresourced"){
                                     ui->checkBox_uresourced->setChecked(true);
                                     }
                                     else
                                         {
                                         ui->checkBox_uresourced->setChecked(false);
                                          }

                                 proc.start("systemctl", QStringList()<< "is-enabled" << "uresourced" << "--no-ask-password");
                                 proc.waitForFinished();
                                 QString uresourcedservice=proc.readAllStandardOutput();
                                 uresourcedservice = uresourcedservice.trimmed();
                                 if (uresourcedservice == "enabled"){
                                     ui->checkBox_uresourcedservice->setChecked(true);
                                     }
                                 else
                                     {
                                        ui->checkBox_uresourcedservice->setChecked(false);
                                      }

                                 proc.start("pacman -Qq irqbalance");
                                  proc.waitForFinished();
                                  QString irqbalance=proc.readAllStandardOutput();
                                  irqbalance = irqbalance.trimmed();
                                  // qDebug()<< irqbalance;
                                  if (irqbalance == "irqbalance"){
                                      ui->checkBox_irqbalance->setChecked(true);
                                      }
                                      else
                                          {
                                          ui->checkBox_irqbalance->setChecked(false);
                                           }

                                  proc.start("systemctl", QStringList()<< "is-enabled" << "irqbalance" << "--no-ask-password");
                                  proc.waitForFinished();
                                  QString irqbalanceservice=proc.readAllStandardOutput();
                                  irqbalanceservice = irqbalanceservice.trimmed();
                                  if (irqbalanceservice == "enabled"){
                                      ui->checkBox_irqbalanceservice->setChecked(true);
                                      }
                                  else
                                      {
                                         ui->checkBox_irqbalanceservice->setChecked(false);
                                       }

                                proc.start("pacman -Qq ananicy-cpp");
                                                                        proc.waitForFinished();
                                                                            QString ananicy=proc.readAllStandardOutput();
                                                                            ananicy = ananicy.trimmed();
                                                                           // qDebug()<< ananicy-cpp;
                                                                        if (ananicy == "ananicy-cpp"){
                                                                            ui->checkBox_ananicy->setChecked(true);
                                                                        }
                                                                        else
                                                                        {
                                                                            ui->checkBox_ananicy->setChecked(false);
                                                                        }

                                                                proc.start("systemctl", QStringList()<< "is-enabled" << "ananicy-cpp" << "--no-ask-password");
                                                                        proc.waitForFinished();
                                                                        QString ananicyservice=proc.readAllStandardOutput();
                                                                        ananicyservice = ananicyservice.trimmed();
                                                                        if (ananicyservice == "enabled"){
                                                                            ui->checkBox_ananicyservice->setChecked(true);
                                                                        }
                                                                        else
                                                                        {
                                                                            ui->checkBox_ananicyservice->setChecked(false);
                                                                        }


                                proc.start("/bin/bash", QStringList()<< "/usr/share/garuda/scripts/check-shell.sh");
                                proc.waitForFinished(10);
                                QString fish=proc.readAllStandardOutput();
                                fish = fish.trimmed();
                                if (fish == "/usr/bin/fish" || fish == "/bin/fish"){
                                         ui->checkBox_fish->setChecked(true);
                                }

                                else
                                {
                                         ui->checkBox_fish->setChecked(false);
                                }

                                proc.start("/bin/bash", QStringList()<< "/usr/share/garuda/scripts/check-shell.sh");
                                proc.waitForFinished(10);
                                QString zsh=proc.readAllStandardOutput();
                                zsh = zsh.trimmed();
                                if (zsh == "/usr/bin/zsh" || zsh == "/bin/zsh")
                                {
                                         ui->checkBox_zsh->setChecked(true);
                                }

                                else
                                {
                                         ui->checkBox_zsh->setChecked(false);
                                }

                                proc.start("/bin/bash", QStringList()<< "/usr/share/garuda/scripts/check-shell.sh");
                                proc.waitForFinished(10);
                                QString bash=proc.readAllStandardOutput();
                                bash = bash.trimmed();
                                if (bash == "/usr/bin/bash" || bash == "/bin/bash")
                                {
                                         ui->checkBox_bash->setChecked(true);
                                }

                                else
                                {
                                         ui->checkBox_bash->setChecked(false);
                                }

                                proc.start("/bin/bash", QStringList()<< "/usr/share/garuda/scripts/check-shell.sh");
                                proc.waitForFinished(10);
                                QString shell=proc.readAllStandardOutput();
                                shell = shell.trimmed();
                                if (shell == "/usr/bin/sh" || shell == "/bin/sh")
                                {
                                         ui->checkBox_sh->setChecked(true);
                                }
                                else
                                {
                                         ui->checkBox_sh->setChecked(false);
                                }
}
/*######################################################################################
 *                                  Maintenance tab                                    *
######################################################################################*/

void GarudaAssistant::on_pushButton_reflector_clicked()
{
    this->hide();
        system("reflector-simple");
        this->show();
}

void GarudaAssistant::on_pushButton_refreshkeyring_clicked()
{
    this->hide();
        system("alacritty --command pkexec bash -c 'pacman-key --init && pacman -Qs archlinux-keyring && pacman-key --populate archlinux & pacman -Qs chaotic-keyring && pacman-key --populate chaotic & pacman -Qs blackarch-keyring && pacman-key --populate blackarch & read p'");
        this->show();
}

void GarudaAssistant::on_pushButton_sysup_clicked()
{
    this->hide();
        system("alacritty --command bash -c 'update && read p'");
        this->show();
}

void GarudaAssistant::on_pushButton_orphans_clicked()
{
    this->hide();
        system("alacritty --command pkexec bash -c 'pacman -Rns $(pacman -Qtdq) && read p'");
        this->show();
}

void GarudaAssistant::on_pushButton_clrcache_clicked()
{
    this->hide();
        system("alacritty --command pkexec bash -c 'paccache -ruk 0 && read p'");
        this->show();
}

void GarudaAssistant::on_pushButton_reinstall_clicked()
{
    this->hide();
        system("alacritty --command pkexec bash -c 'pacman -S $(pacman -Qnq) && read p'");
        this->show();
}

void GarudaAssistant::on_pushButton_dblck_clicked()
{
    this->hide();
        system("alacritty --command pkexec bash -c 'rm /var/lib/pacman/db.lck && read p'");
        this->show();
}

void GarudaAssistant::on_pushButton_editrepo_clicked()
{
    this->hide();
        system("pace");
        this->show();
}

void GarudaAssistant::on_pushButton_clrlogs_clicked()
{
    QProcess proc;
                proc.start("/bin/bash", QStringList()<< "/usr/share/garuda/scripts/clear-cache.sh");
                proc.waitForFinished();
}

void GarudaAssistant::on_pushButton_clicked()
{
    system("cp -rf /etc/skel/. ~/ && notify-send -a \"Garuda Asssistant\" \"Configs applied, please relogin to finish!\"");
}

/*######################################################################################
 *                                    BTRFS tab                                        *
######################################################################################*/






void GarudaAssistant::on_checkBox_Trim_clicked(bool checked)
{
    if (checked)
       {
          system("systemctl enable --now btrfs-trim.timer ");
       }
       else
       {
          system("systemctl disable --now btrfs-trim.timer");
       }
       check_status();
       check_status();
}

void GarudaAssistant::on_checkBox_Scrub_clicked(bool checked)
{
    if (checked)
        {
           system("systemctl enable --now btrfs-scrub.timer ");
        }
        else
        {
           system("systemctl disable --now btrfs-scrub.timer");
        }
        check_status();
        check_status();
}

void GarudaAssistant::on_checkBox_Defrag_clicked(bool checked)
{
    if (checked)
        {
           system("systemctl enable --now btrfs-defrag.timer ");
        }
        else
        {
           system("systemctl disable --now btrfs-defrag.timer");
        }
        check_status();
        check_status();
}

void GarudaAssistant::on_checkBox_Balance_clicked(bool checked)
{
    if (checked)
        {
           system("systemctl enable --now btrfs-balance.timer ");
        }
        else
        {
           system("systemctl disable --now btrfs-balance.timer");
        }
        check_status();
        check_status();
}

void GarudaAssistant::on_pushButton_balance_clicked()
{
    this->hide();
        system("alacritty --command pkexec btrfs balance start / --full-balance --verbose");
        this->show();
}

void GarudaAssistant::on_toolButton_diskRefresh_clicked()
{
    QProcess process;
        process.start("btrfs filesystem usage / ");
        process.waitForReadyRead();
        ui->textEdit->setText(process.readAll());
        process.waitForFinished();
}

/*######################################################################################
 *                           Settings tab                                              *
######################################################################################*/

void GarudaAssistant::on_checkBox_guest_clicked(bool checked)
{
    if (checked)
        {
            system("alacritty --command pkexec pacman -S systemd-guest-user ");
        }
        else
        {
            system("alacritty --command pkexec pacman -Rns systemd-guest-user ");
        }
        check_status();
        check_status();
}

void GarudaAssistant::on_checkBox_adguard_clicked(bool checked)
{
    if (checked)
    {
        QProcess proc;
                proc.start("pkexec", QStringList()<< "/bin/bash" << "/usr/share/garuda/scripts/adguard-on.sh");
                proc.waitForFinished();
    }
    else
    {
        QProcess proc;
                proc.start("pkexec", QStringList()<< "/bin/bash" << "/usr/share/garuda/scripts/adguard-off.sh");
                proc.waitForFinished();
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_printer_clicked(bool checked)
{
    if (checked)
    {
        system("alacritty --command pkexec pacman -S printer-support ");
    }
    else
    {
        system("alacritty --command pkexec pacman -Rns printer-support ");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_cups_clicked(bool checked)
{
    if (checked)
    {
       system("systemctl enable --now cups.socket --now");
    }
    else
    {
       system("systemctl disable --now cups.socket");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_scanning_clicked(bool checked)
{
    if (checked)
    {
        system("alacritty --command pkexec pacman -S scanner-support ");
    }
    else
    {
        system("alacritty --command pkexec pacman -Rns scanner-support ");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_usbipp_clicked(bool checked)
{
    if (checked)
    {
       system("systemctl enable --now ipp-usb ");
    }
    else
    {
       system("systemctl disable --now ipp-usb");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_saned_clicked(bool checked)
{
    if (checked)
    {
       system("systemctl enable --now saned.socket");
    }
    else
    {
       system("systemctl disable --now saned.socket");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_samba_clicked(bool checked)
{
    if (checked)
    {
        system("alacritty --command pkexec pacman -S samba-support ");
    }
    else
    {
        system("alacritty --command pkexec pacman -Rns samba-support  ");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_sambashare_clicked(bool checked)
{
    if (checked)
    {
       system("pkexec gpasswd sambashare -a $USER");
    }
    else
    {
       system("pkexec gpasswd sambashare -d $USER");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_smb_clicked(bool checked)
{
    if (checked)
    {
       system("systemctl enable --now smb");
    }
    else
    {
       system("systemctl disable --now smb");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_nmb_clicked(bool checked)
{
    if (checked)
    {
       system("systemctl enable --now nmb");
    }
    else
    {
       system("systemctl disable --now nmb");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_firewalld_clicked(bool checked)
{
    if (checked)
    {
        system("alacritty --command pkexec pacman -S firewalld python-pyqt5 ");
    }
    else
    {
        system("alacritty --command pkexec pacman -Rns firewalld python-pyqt5 ");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_firewalldservice_clicked(bool checked)
{
    if (checked)
    {
       system("systemctl enable --now firewalld");
    }
    else
    {
       system("systemctl disable --now firewalld");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_ufw_clicked(bool checked)
{
    if (checked)
    {
        system("alacritty --command pkexec pacman -S ufw ");
    }
    else
    {
        system("alacritty --command pkexec pacman -Rns ufw  ");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_ufwservice_clicked(bool checked)
{
    if (checked)
    {
       system("systemctl enable --now ufw");
    }
    else
    {
       system("systemctl disable --now ufw");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_fcitx_clicked(bool checked)
{
    if (checked)
    {
        system("alacritty --command pkexec pacman -S fcitx-input-support ");
    }
    else
    {
        system("alacritty --command pkexec pacman -Rns fcitx-input-support ");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_fcitx5_clicked(bool checked)
{
    if (checked)
    {
        system("alacritty --command pkexec pacman -S fcitx5-input-support ");
    }
    else
    {
        system("alacritty --command pkexec pacman -Rns fcitx5-input-support ");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_ibus_clicked(bool checked)
{
    if (checked)
    {
        system("alacritty --command pkexec pacman -S ibus-input-support ");
    }
    else
    {
        system("alacritty --command pkexec pacman -Rns ibus-input-support ");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_asian_clicked(bool checked)
{
    if (checked)
    {
        system("alacritty --command pkexec pacman -S asian-fonts ");
    }
    else
    {
        system("alacritty --command pkexec pacman -Rns asian-fonts ");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_lptrce_clicked(bool checked)
{
    if (checked)
    {
        system("alacritty --command pkexec pacman -S evdev-right-click-emulation ");
    }
    else
    {
        system("alacritty --command pkexec pacman -Rns evdev-right-click-emulation ");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_evdevrce_clicked(bool checked)
{
    if (checked)
    {
       system("systemctl enable --now evdev-rce");
    }
    else
    {
       system("systemctl disable --now evdec-rce");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_virtmanager_clicked(bool checked)
{
    if (checked)
    {
        system("alacritty --command pkexec pacman -S virt-manager-meta ");
    }
    else
    {
        system("alacritty --command pkexec pacman -Rns virt-manager-meta ");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_libvirtd_clicked(bool checked)
{
    if (checked)
    {
       system("systemctl enable --now libvirtd");
    }
    else
    {
       system("systemctl disable --now libvirtd");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_libvirtgrp_clicked(bool checked)
{
    if (checked)
    {
       system("pkexec gpasswd libvirt -a $USER");
    }
    else
    {
       system("pkexec gpasswd libvirt -d $USER");
    }
    check_status();
    check_status();

}

void GarudaAssistant::on_checkBox_virtualbox_clicked(bool checked)
{
    if (checked)
    {
        system("alacritty --command pkexec pacman -S virtualbox-meta ");
    }
    else
    {
        system("alacritty --command pkexec pacman -Rns virtualbox-meta ");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_vboxusers_clicked(bool checked)
{
    if (checked)
    {
       system("pkexec gpasswd vboxusers -a $USER");
    }
    else
    {
       system("pkexec gpasswd vboxusers -d $USER");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_hidpi_clicked(bool checked)
{
    if (checked)
    {
        system("alacritty --command pkexec env DISPLAY=$DISPLAY XAUTHORITY=$XAUTHORITY /usr/share/garuda/scripts/hidpi-on.sh");
        system("notify-send -i 'garuda' 'Hidpi-on' 'Changes made.Reboot system for changes to take effect to take effect'");
    }
    else
    {
        system("alacritty --command pkexec env DISPLAY=$DISPLAY XAUTHORITY=$XAUTHORITY /usr/share/garuda/scripts/hidpi-off.sh");
        system("notify-send -i 'garuda' 'Hidpi-off' 'Changes made.Reboot system for changes to take effect to take effect'");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_gdm_clicked(bool checked)
{
    if (checked)
    {
        QProcess proc;
                proc.start("pkexec", QStringList()<< "/bin/bash" << "/usr/share/garuda/scripts/gdmwayland-on.sh");
                proc.waitForFinished();
    }
    else
    {
        QProcess proc;
                proc.start("pkexec", QStringList()<< "/bin/bash" << "/usr/share/garuda/scripts/gdmwayland-off.sh");
                proc.waitForFinished();
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_modemservice_clicked(bool checked)
{
    if (checked)
    {
       system("systemctl enable --now ModemManager");
    }
    else
    {
       system("systemctl disable --now ModemManager");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_gpsd_clicked(bool checked)
{
    if (checked)
    {
       system("systemctl enable --now gpsd");
    }
    else
    {
       system("systemctl disable --now gpsd");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_bluetooth_clicked(bool checked)
{
    if (checked)
    {
        system("alacritty --command pkexec pacman -S bluetooth-support ");
    }
    else
    {
        system("alacritty --command pkexec pacman -Rns bluetooth-support ");
    }
    check_status();
    check_status();

}

void GarudaAssistant::on_checkBox_bluetoothservice_clicked(bool checked)
{
    if (checked)
    {
       system("systemctl enable --now bluetooth");
    }
    else
    {
       system("systemctl disable --now bluetooth");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_bluetoothauto_clicked(bool checked)
{
    if (checked)
    {
       system("systemctl enable --now bluetooth-autoconnect ");
    }
    else
    {
       system("systemctl disable --now bluetooth-autoconnect");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_pulsebluetoothauto_clicked(bool checked)
{
    if (checked)
    {
       system("systemctl enable pulseaudio-bluetooth-autoconnect --user --now");
    }
    else
    {
       system("systemctl disable pulseaudio-bluetooth-autoconnect --user");
    }
    check_status();
    check_status();
}



void GarudaAssistant::on_checkBox_gstreamer_clicked(bool checked)
{
    if (checked)
    {
        system("alacritty --command pkexec pacman -S gstreamer-meta ");
    }
    else
    {
        system("alacritty --command pkexec pacman -Rns gstreamer-meta ");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_network_clicked(bool checked)
{
    if (checked)
    {
        system("alacritty --command pkexec pacman -S networkmanager-support ");
    }
    else
    {
        system("alacritty --command pkexec pacman -Rns networkmanager-support ");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_networkservice_clicked(bool checked)
{
    if (checked)
    {
       system("systemctl enable --now --force NetworkManager");
    }
    else
    {
       system("systemctl disable --now NetworkManager");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_connman_clicked(bool checked)
{
    if (checked)
    {
        system("alacritty --command pkexec pacman -S connman-support ");
    }
    else
    {
        system("alacritty --command pkexec pacman -Rns connman-support ");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_connmanservice_clicked(bool checked)
{
    if (checked)
    {
       system("systemctl enable --force --now connman ");
    }
    else
    {
       system("systemctl disable connman");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_modemmanager_clicked(bool checked)
{
    if (checked)
    {
       system("systemctl enable --force --now ModemManager");
    }
    else
    {
       system("systemctl disable --now ModenManager");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_ofono_clicked(bool checked)
{
    if (checked)
    {
       system("systemctl enable --force --now ofono");
    }
    else
    {
       system("systemctl disable --now ofono");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_neard_clicked(bool checked)
{
    if (checked)
    {
       system("systemctl enable --force --now neard");
    }
    else
    {
       system("systemctl disable --now neard");
    }
    check_status();
    check_status();
}


void GarudaAssistant::on_checkBox_hblock_clicked(bool checked)
{
    if (checked)
    {
        QProcess proc;
                proc.start("/bin/bash", QStringList()<< "/usr/share/garuda/scripts/hblock-on.sh");
                proc.waitForFinished();
    }
    else
    {
        QProcess proc;
                proc.start("/bin/bash", QStringList()<< "/usr/share/garuda/scripts/hblock-off.sh");
                proc.waitForFinished();
    }
    check_status();
    check_status();
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_alsa_clicked(bool checked)
{
    if (checked)
    {
        system("alacritty --command pkexec pacman -S alsa-support ");
    }
    else
    {
        system("alacritty --command pkexec pacman -Rns alsa-support ");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_jack_clicked(bool checked)
{
    if (checked)
    {
        system("alacritty --command pkexec pacman -S jack-support ");
    }
    else
    {
        system("alacritty --command pkexec pacman -Rns jack-support ");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_pulseaudio_clicked(bool checked)
{
    if (checked)
    {
        system("alacritty --command pkexec pacman -S pulseaudio-support ");
    }
    else
    {
        system("alacritty --command pkexec pacman -Rns pulseaudio-support ");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_pipewire_clicked(bool checked)
{
    if (checked)
    {
        system("alacritty --command pkexec pacman -S pipewire-support ");
    }
    else
    {
        system("alacritty --command pkexec pacman -Rns pipewire-support ");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_lp_clicked(bool checked)
{
    if (checked)
    {
       system("pkexec gpasswd lp -a $USER");
    }
    else
    {
       system("pkexec gpasswd lp -d $USER");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_cupsgrp_clicked(bool checked)
{
    if (checked)
    {
       system("pkexec gpasswd cups -a $USER");
    }
    else
    {
       system("pkexec gpasswd cups -d $USER");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_video_clicked(bool checked)
{
    if (checked)
    {
       system("pkexec gpasswd video -a $USER");
    }
    else
    {
       system("pkexec gpasswd video -d $USER");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_realtime_clicked(bool checked)
{
    if (checked)
    {
       system("pkexec gpasswd realtime -a $USER");
    }
    else
    {
       system("pkexec gpasswd realtime -d $USER");
    }
    check_status();
    check_status();
}



void GarudaAssistant::on_checkBox_performancetweaks_clicked(bool checked)
{
    if (checked)
    {
        system("alacritty --command pkexec pacman -S performance-tweaks ");
    }
    else
    {
        system("alacritty --command pkexec pacman -Rns performance-tweaks ");
    }
    check_status();
    check_status();
}


void GarudaAssistant::on_checkBox_sysgrp_clicked(bool checked)
{
    if (checked)
    {
       system("pkexec gpasswd sys -a $USER");
    }
    else
    {
       system("pkexec gpasswd sys -d $USER");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_scannergrp_clicked(bool checked)
{
    if (checked)
    {
       system("pkexec gpasswd scanner -a $USER");
    }
    else
    {
       system("pkexec gpasswd scanner -d $USER");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_tlp_clicked(bool checked)
{
    if (checked)
    {
        system("alacritty --command pkexec pacman -S tlp ");
    }
    else
    {
        system("alacritty --command pkexec pacman -Rns tlp ");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_autocpufreq_clicked(bool checked)
{
    if (checked)
    {
        system("alacritty --command pkexec pacman -S auto-cpufreq ");
    }
    else
    {
        system("alacritty --command pkexec pacman -Rns auto-cpufreq ");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_thermald_clicked(bool checked)
{
    if (checked)
    {
        system("alacritty --command pkexec pacman -S thermald ");
    }
    else
    {
        system("alacritty --command pkexec pacman -Rns thermald ");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_intelundervolt_clicked(bool checked)
{
    if (checked)
    {
        system("alacritty --command pkexec pacman -S intel-undervolt ");
    }
    else
    {
        system("alacritty --command pkexec pacman -Rns intel-undervolt ");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_tlpservice_clicked(bool checked)
{
    if (checked)
    {
       system("systemctl enable --now tlp");
    }
    else
    {
       system("systemctl disable --now tlp");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_autocpufreqservice_clicked(bool checked)
{
    if (checked)
    {
       system("systemctl enable --now auto-cpufreq");
    }
    else
    {
       system("systemctl disable --now auto-cpufreq");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_thermaldservice_clicked(bool checked)
{
    if (checked)
    {
       system("systemctl enable --now thermald");
    }
    else
    {
       system("systemctl disable --now thermald");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_intelundervoltservice_clicked(bool checked)
{
    if (checked)
    {
       system("systemctl enable --now intel-undervolt");
    }
    else
    {
       system("systemctl disable --now intel-undervolt");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_powersavetweaks_clicked(bool checked)
{
    if (checked)
    {
        system("alacritty --command pkexec pacman -S powersave-tweaks ");
    }
    else
    {
        system("alacritty --command pkexec pacman -Rns powersave-tweaks ");
    }
    check_status();
    check_status();
}

void GarudaAssistant::on_pushButton_2_clicked()
{
    check_status();
}

void GarudaAssistant::on_pushButton_3_clicked()
{
    check_status();
}



void GarudaAssistant::on_checkBox_fish_clicked(bool checked)
{
    if (checked)
    {
        if (!checkAndInstall("garuda-fish-config"))
                return;
        system("alacritty --command chsh -s /bin/fish");
        system("cp /etc/skel/.config/fish/config.fish ~/.config/fish/config.fish && notify-send -a \"Garuda Asssistant\" \"Shell Changed, please relogin to apply changes!\"");
    }
    check_status();
    check_status();
    check_status();
    check_status();

}





void GarudaAssistant::on_checkBox_zsh_clicked(bool checked)
{
    if (checked)
    {
        if (!checkAndInstall("garuda-zsh-config"))
                return;
        system("alacritty --command chsh -s /bin/zsh");
        system("cp /etc/skel/.zshrc ~ && notify-send -a \"Garuda Asssistant\" \"Shell Changed, please relogin to apply changes!\"");
    }
    check_status();
    check_status();
    check_status();
    check_status();

}

void GarudaAssistant::on_checkBox_bash_clicked(bool checked)
{
    if (checked)
    {
        if (!checkAndInstall("bash"))
                return;
        system("alacritty --command chsh -s /bin/bash");
        system("notify-send -a \"Garuda Asssistant\" \"Shell Changed, please relogin to apply changes!\"");
    }
    check_status();
    check_status();
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_sh_clicked(bool checked)
{
    if (checked)
    {
        system("alacritty --command chsh -s /bin/sh");
        system("notify-send -a \"Garuda Asssistant\" \"Shell Changed, please relogin to apply changes!\"");
    }
    check_status();
    check_status();
    check_status();
    check_status();
}

void GarudaAssistant::on_checkBox_ananicy_clicked(bool checked)
{
    if (checked)
    {
        system("alacritty --command pkexec pacman -S ananicy-cpp ");
    }
    else
    {
        system("alacritty --command pkexec pacman -Rns ananicy-cpp ");
    }
    check_status();
    check_status();
}


void GarudaAssistant::on_checkBox_uresourced_clicked(bool checked)
{
    if (checked)
    {
        system("alacritty --command pkexec pacman -S uresourced ");
    }
    else
    {
        system("alacritty --command pkexec pacman -Rns uresourced ");
    }
    check_status();
    check_status();
}


void GarudaAssistant::on_checkBox_irqbalance_clicked(bool checked)
{
    if (checked)
    {
        system("alacritty --command pkexec pacman -S irqbalance ");
    }
    else
    {
        system("alacritty --command pkexec pacman -Rns irqbalance ");
    }
    check_status();
    check_status();
}


void GarudaAssistant::on_checkBox_ananicyservice_clicked(bool checked)
{
    if (checked)
    {
       system("systemctl enable --now ananicy-cpp");
    }
    else
    {
       system("systemctl disable --now ananicy-cpp");
    }
    check_status();
    check_status();
}


void GarudaAssistant::on_checkBox_uresourcedservice_clicked(bool checked)
{
    if (checked)
    {
       system("systemctl enable --now uresourced");
    }
    else
    {
       system("systemctl disable --now uresourced");
    }
    check_status();
    check_status();
}


void GarudaAssistant::on_checkBox_irqbalanceservice_clicked(bool checked)
{
    if (checked)
    {
       system("systemctl enable --now irqbalance");
    }
    else
    {
       system("systemctl disable --now irqbalance");
    }
    check_status();
    check_status();
}

